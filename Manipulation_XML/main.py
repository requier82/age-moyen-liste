from xml.dom import minidom
#

if __name__ == '__main__':
#parser un document XML
    doc = minidom.parse('contacts.xml')
    print(doc)
#Parcourir un arbre DOM
    #Indique si un noeud a des fils
    print(doc.hasChildNodes())
    #Donne accès aux fils d'un noeud (sous forme de liste)
    print(doc.childNodes)
    print(doc.childNodes[0].hasChildNodes())
    #fournit le noeud racine d'un document DOM
    root = doc.documentElement
    print(root)
    #Donnent accès au premier et dernier fils d'un noeud
    print(root.childNodes)
    print(root.firstChild)
    print(root.lastChild)
    #donnent respectivement accès au fils suivant et au fils précédent (ou None si plus de fils). Le calcul se fait par rapport à une racine commune
    current = root.firstChild
    while current:
        print(current)
        current = current.nextSibling
    #Donne accès aux parents directs d'un noeud
#..  doctest::
    print("root",root)
    print(root.firstChild)
    print(root.firstChild.parentNode)